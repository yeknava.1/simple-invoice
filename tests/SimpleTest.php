<?php

use Carbon\Carbon;
use Orchestra\Testbench\TestCase;
use Tests\OwnerModel;
use Tests\UserModel;
use Yeknava\SimpleInvoice\Exceptions\ExpiredInvoiceException;
use Yeknava\SimpleInvoice\InvoiceItem;

class SimpleTest extends TestCase
{
    /**
     * Setup the test environment.
     */
    protected function setUp() : void
    {
        parent::setUp();

        $this->loadMigrationsFrom(__DIR__ . '/../migrations', ['--database' => 'testing']);
        $this->loadMigrationsFrom(__DIR__ . '/migrations', ['--database' => 'testing']);
    }
    /**
     * Define environment setup.
     *
     * @param  \Illuminate\Foundation\Application  $app
     * @return void
     */
    protected function getEnvironmentSetUp($app)
    {
        $app['config']->set('database.default', 'testing');
        $app['config']->set('simple-invoice', require_once(__DIR__ . '/../config/simple-invoice.php'));
    }

    protected function getPackageProviders($app)
    {
        return [
            \Yeknava\SimpleInvoice\SimpleInvoiceServiceProvider::class,
        ];
    }

    public function test()
    {        
        $user1 = (new UserModel([]));
        $user1->save();
        $user2 = (new OwnerModel([]));
        $user2->save();

        $invoice = $user1->newBill('title');
        $invoice->setOwner($user2);
        $invoice->addItem((new InvoiceItem(1000, 'item1'))
            ->setTax(100)
            ->setItem($user2)
            ->setQuantity(5)
            ->setShippingPrice(100)
            ->setDiscountInPercent(10));
        $invoice->addItem((new InvoiceItem(1000, 'item2'))
            ->setTax(100)
            ->setItem($user2)
            ->setQuantity(5)
            ->setShippingPrice(100)
            ->setDiscountInPercent(10));
        
        $generatedInvoice = $invoice->setExpiredDate(Carbon::now())->generate();
        try {
            $generatedInvoice->paid();
        } catch (ExpiredInvoiceException $e) {
            $this->assertTrue(true);
        }

        $invoice = $invoice->setExpiredDate(Carbon::now()->addDay())->generate();
        $invoice = $invoice->paid();

        $this->assertEquals($invoice->tax, 200);
        $this->assertEquals($invoice->shipping, 200);
        $this->assertEquals($invoice->discount, 1000);

        $this->assertEquals(count($user1->bills), 2);
        $this->assertEquals(count($user2->invoices), 2);
    }
}