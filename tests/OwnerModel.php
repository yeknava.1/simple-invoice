<?php

namespace Tests;

use Illuminate\Database\Eloquent\Model;
use Yeknava\SimpleInvoice\InvoiceOwner;
use Illuminate\Database\Eloquent\SoftDeletes;

class OwnerModel extends Model
{
    use InvoiceOwner, SoftDeletes;

    protected $table = 'simple_invoice_test_users';
}
