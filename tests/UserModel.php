<?php

namespace Tests;

use Illuminate\Database\Eloquent\Model;
use Yeknava\SimpleInvoice\Biller;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserModel extends Model
{
    use Biller, SoftDeletes;

    protected $table = 'simple_invoice_test_users';
}
