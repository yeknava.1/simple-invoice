<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSimpleInvoiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(config('simple-invoice.invoice_table'), function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title')->nullable();
            $table->nullableMorphs('biller');
            $table->nullableMorphs('owner');
            $table->double('amount', 16, 4)->default(0);
            $table->double('tax', 16, 4)->default(0);
            $table->double('shipping')->default(0);
            $table->double('discount', 16, 4)->default(0);
            $table->string('payment_type')->nullable();
            $table->text('terms')->nullable();
            $table->text('description')->nullable();
            $table->text('logo_path')->nullable();
            $table->jsonb('extra')->nullable();
            $table->timestamp('paid_at')->nullable();
            $table->timestamp('expired_at')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(config('simple-invoice.invoice_table'));
    }
}
