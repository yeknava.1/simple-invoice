<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSimpleInvoiceItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(config('simple-invoice.invoice_items_table'), function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('invoice_id')->unsigned()->nullable()->index();
            $table->foreign('invoice_id')
                ->references('id')
                ->on(config('simple-invoice.invoice_table'));
            $table->nullableMorphs('item');
            $table->string('title')->nullable();
            $table->integer('quantity')->nullable();
            $table->double('unit_price', 16, 4)->nullable();
            $table->double('amount', 16, 4)->nullable();
            $table->double('tax', 16, 4)->default(0);
            $table->double('shipping_price')->default(0);
            $table->double('discount', 16, 4)->default(0);
            $table->text('image_path')->nullable();
            $table->text('description')->nullable();
            $table->jsonb('extra')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(config('simple-invoice.items_table'));
    }
}
