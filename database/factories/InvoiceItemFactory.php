<?php

namespace Yeknava\SimpleInvoiceDatabase\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

use Yeknava\SimpleInvoice\Models\SimpleInvoice;
use Yeknava\SimpleInvoice\Models\SimpleInvoiceItem;

class InvoiceItemFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = SimpleInvoiceItem::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'invoice_id' => SimpleInvoice::factory(),
            'item_id' => null,
            'item_type' => null,
            'title' => $this->faker->sentence(),
            'quantity' => rand(1, 10),
            'unit_price' => rand(1, 9) * (10 ^ rand(3, 5)),
            'amount' => rand(1, 9) * (10 ^ rand(3, 5)),
            'tax' => rand(1, 9) * (10 ^ rand(2, 3)),
            'shipping_price' => 0,
            'discount' => 0,
            'image_path' => null,
            'description' => null,
            'extra' => null,
        ];
    }
}
