<?php

namespace Yeknava\SimpleInvoiceDatabase\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

use Yeknava\SimpleInvoice\Models\SimpleInvoice;

class InvoiceFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = SimpleInvoice::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'title' => $this->faker->sentence(),
            'owner_id' => null,
            'owner_type' => null,
            'biller_id' => null,
            'biller_type' => null,
            'amount' => rand(1, 9) * (10 ^ rand(3, 5)),
            'tax' => rand(1, 9) * (10 ^ rand(2, 3)),
            'shipping' => rand(1, 9) * (10 ^ rand(2, 3)),
            'discount' => rand(1, 9) * (10 ^ rand(2, 3)),
            'payment_type' => null,
            'terms' => null,
            'description' => null,
            'logo_path' => null,
            'extra' => null,
            'paid_at' => rand(0, 1) ? now() : null,
            'expired_at' => null,
        ];
    }
}
