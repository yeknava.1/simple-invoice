<?php

namespace Yeknava\SimpleInvoice;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Throwable;

class Invoice
{
    public $title;
    public $description;

    protected $biller;
    protected $biller_id;
    protected $biller_type;

    protected $owner;
    protected $owner_id;
    protected $owner_type;

    public $terms;
    public $logoPath;
    public $expiredAt;
    public $extra;
    private $items;

    public function __construct(string $title = null)
    {
        $this->title = $title;
        $this->extra = [];
        $this->items = [];
    }

    public function setBiller(Model $biller) : self
    {
        $this->biller = $biller;

        return $this;
    }

    public function setBillerData(string $biller_id, string $biller_type) : self
    {
        $this->biller_id = $biller_id;
        $this->biller_type = $biller_type;

        return $this;
    }

    public function setOwner(Model $owner) : self
    {
        $this->owner = $owner;

        return $this;
    }

    public function setOwnerData(string $owner_id, string $owner_type) : self
    {
        $this->owner_id = $owner_id;
        $this->owner_type = $owner_type;

        return $this;
    }

    public function setTitle(array $title) : self
    {
        $this->title = $title;

        return $this;
    }

    public function setDescription(array $description) : self
    {
        $this->description = $description;

        return $this;
    }

    public function setLogo(string $logoPath) : self
    {
        $this->logoPath = $logoPath;

        return $this;
    }

    public function setExpiredDate(Carbon $expiredAt) : self
    {
        $this->expiredAt = $expiredAt;

        return $this;
    }

    public function setTerms(string $terms) : self
    {
        $this->terms = $terms;

        return $this;
    }

    public function setExtra(array $extra) : self
    {
        $this->extra = $extra;

        return $this;
    }

    public function addItem(InvoiceItem $item) : self
    {
        $this->items[] = $item;

        return $this;
    }

    public function generate() : SimpleInvoice
    {
        try {
            app('db')->beginTransaction();
            $invoice = new SimpleInvoice([
                'title' => $this->title,
                'description' => $this->description
            ]);

            $amount = 0;
            $discount = 0;
            $tax = 0;
            $shipping = 0;

            $invoiceItems = [];
            foreach($this->items as $item) {
                $itemArray = $item->toArray();
                $amount += $itemArray['amount'];
                $discount += $itemArray['discount'];
                $tax += $itemArray['tax'];
                $shipping += $itemArray['shipping_price'];
                $invoiceItem = new SimpleInvoiceItem($itemArray);
                if ($itemArray['item']) {
                    $invoiceItem->item()->associate($itemArray['item']);
                }
                $invoiceItems[] = $invoiceItem;
            }

            if (!empty($this->owner)) {
                $invoice->owner()->associate($this->owner);
            } elseif (isset($this->owner_id, $this->owner_type)) {
                $invoice->owner_id = $this->owner_id;
                $invoice->owner_type = $this->owner_type;
            }

            if (!empty($this->biller)) {
                $invoice->biller()->associate($this->biller);
            } elseif (isset($this->biller_id, $this->biller_type)) {
                $invoice->biller_id = $this->biller_id;
                $invoice->biller_type = $this->biller_type;
            }

            $invoice->amount = $amount;
            $invoice->discount = $discount;
            $invoice->tax = $tax;
            $invoice->shipping = $shipping;
            $invoice->terms = $this->terms;
            $invoice->shipping = $shipping;
            $invoice->logo_path = $this->logoPath;
            $invoice->expired_at = $this->expiredAt ?? null;
            $invoice->save();
            $invoice->items()->saveMany($invoiceItems);
            app('db')->commit();

            return $invoice;
        } catch (Throwable $e) {
            app('db')->rollback();
            throw $e;
        }
    }
}
