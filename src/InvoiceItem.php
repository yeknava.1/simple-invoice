<?php

namespace Yeknava\SimpleInvoice;

use Illuminate\Database\Eloquent\Model;
use Yeknava\SimpleInvoice\Exceptions\InvalidDiscountException;
use Yeknava\SimpleInvoice\Exceptions\InvalidQuantityException;
use Yeknava\SimpleInvoice\Exceptions\InvalidTaxException;

class InvoiceItem
{
    protected $title;
    protected $unitPrice;

    public $description;
    public $imagePath;
    public $shippingFee;
    public $item;

    protected $quantity = 1;
    protected $tax = 0;
    protected $discount = 0;
    protected $extra = [];

    private $discountIsPercent = false;
    private $taxIsPercent = false;

    public function __construct(float $unitPrice, string $title = null)
    {
        $this->unitPrice = $unitPrice;
        $this->title = $title;
    }

    public static function make(float $unitPrice, string $title = null)
    {
        return new static($unitPrice, $title);
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function setQuantity(int $quantity): self
    {
        if ($quantity < 1) throw new InvalidQuantityException();

        $this->quantity = $quantity;

        return $this;
    }

    public function setTax(float $tax): self
    {
        $this->tax = $tax;

        return $this;
    }

    public function setTaxInPercent(float $tax): self
    {
        if ($tax < 0 || $tax > 100) {
            throw new InvalidTaxException();
        }

        $this->tax = $tax;
        $this->taxIsPercent = true;

        return $this;
    }

    public function setDiscountAmount(float $discount): self
    {
        $this->discount = $discount;
        $this->discountIsPercent = false;

        return $this;
    }

    public function setDiscountInPercent(float $discount): self
    {
        if ($discount < 0 || $discount > 100) {
            throw new InvalidDiscountException();
        }

        $this->discount = $discount;
        $this->discountIsPercent = true;

        return $this;
    }

    public function setShippingFeePrice(float $shipping): self
    {
        $this->shippingFee = $shipping;

        return $this;
    }

    public function setDescription(string $description) : self
    {
        $this->description = $description;

        return $this;
    }

    public function setImagePath(string $imagePath) : self
    {
        $this->imagePath = $imagePath;

        return $this;
    }

    public function setItem(Model $item) :self
    {
        $this->item = $item;

        return $this;
    }

    public function setExtra(array $extra) : self
    {
        $this->extra = $extra;

        return $this;
    }

    public function calculateDiscount(float $amount) : float
    {
        if ($this->discountIsPercent) {
            return ($amount * $this->discount) / 100;
        } else {
            return $this->discount;
        }
    }

    public function calculateTax(float $amount) : float
    {
        if ($this->taxIsPercent) {
            return ($amount * $this->tax) / 100;
        } else {
            return $this->tax;
        }
    }

    public function calculateAmount() : float
    {
        $amount = $this->quantity*$this->unitPrice;
        return ($amount)
            - $this->calculateDiscount($amount)
            + $this->calculateTax($amount)
            + $this->shippingFee;
    }

    public function toArray() : array
    {
        $amount = $this->quantity*$this->unitPrice;

        return [
            'title' => $this->title,
            'quantity' => $this->quantity,
            'unit_price' => $this->unitPrice,
            'discount' => $this->calculateDiscount($amount) ?? 0,
            'tax' => $this->calculateTax($amount) ?? 0,
            'shipping_price' => $this->shippingFee ?? 0,
            'amount' => $this->calculateAmount(),
            'quantity' => $this->quantity,
            'item' => $this->item,
            'description' => $this->description,
            'extra' => $this->extra
        ];
    }
}
