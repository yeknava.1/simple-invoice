<?php

namespace Yeknava\SimpleInvoice;

use Illuminate\Support\ServiceProvider;

class SimpleInvoiceServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->mergeConfigFrom(__DIR__.'/../config/simple-invoice.php', 'simple-invoice');
        $this->publishes([
            __DIR__.'/../config/simple-invoice.php' => config_path('simple-invoice.php'),
        ], 'simple-invoice-config');

        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        $this->publishes([
            __DIR__.'/../database/migrations' => database_path('/migrations'),
        ], 'simple-invoice-migrations');
    }
}
