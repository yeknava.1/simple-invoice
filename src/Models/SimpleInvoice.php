<?php

namespace Yeknava\SimpleInvoice\Models;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;
use Yeknava\SimpleInvoiceDatabase\Factories\InvoiceFactory;
use Yeknava\SimpleInvoice\Exceptions\ExpiredInvoiceException;

class SimpleInvoice extends Model
{
    use HasFactory,
        SoftDeletes;

    protected static function newFactory(): Factory
    {
        return InvoiceFactory::new();
    }

    public function getTable()
    {
        return $this->table = config('simple-invoice.invoice_table');
    }

    protected $fillable = [
        'title', 'description'
    ];

    protected $casts = [
        'paid_at' => 'datetime',
        'expired_at' => 'datetime',
        'extra' => 'array',
    ];

    public function biller()
    {
        return $this->morphTo();
    }

    public function owner()
    {
        return $this->morphTo();
    }

    public function items()
    {
        return $this->hasMany(SimpleInvoiceItem::class, 'invoice_id');
    }

    public function paid(string $paymentType = null) : self
    {
        if (!empty($this->expired_at) && $this->expired_at->lessThan(Carbon::now())) {
            throw new ExpiredInvoiceException();
        }
        $this->payment_type = $paymentType;
        $this->paid_at = Carbon::now();
        $this->save();

        return $this;
    }
}
