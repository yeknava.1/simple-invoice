<?php

namespace Yeknava\SimpleInvoice\Models\Traits;

trait InvoiceOwner
{
    public function invoices()
    {
        return $this->morphMany(SimpleInvoice::class, 'owner');
    }

    public function newInvoice(string $title = null): Invoice
    {
        $invoice = new Invoice($title);

        $invoice->setOwner($this);

        return $invoice;
    }
}
