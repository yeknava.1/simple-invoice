<?php

namespace Yeknava\SimpleInvoice\Models\Traits;

trait Biller
{
    public function bills()
    {
        return $this->morphMany(SimpleInvoice::class, 'biller');
    }

    public function newBill(string $title = null): Invoice
    {
        $invoice = new Invoice($title);

        $invoice->setBiller($this);

        return $invoice;
    }
}
