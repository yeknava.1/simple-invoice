<?php

namespace Yeknava\SimpleInvoice\Models;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;

use Illuminate\Database\Eloquent\Factories\Factory;
use Yeknava\SimpleInvoiceDatabase\Factories\InvoiceItemFactory;

class SimpleInvoiceItem extends Model
{
    use HasFactory;

    protected static function newFactory(): Factory
    {
        return InvoiceItemFactory::new();
    }

    public function getTable()
    {
        return $this->table = config('simple-invoice.invoice_items_table');
    }

    protected $fillable = [
        'amount', 'tax', 'title',
        'quantity', 'unit_price', 'extra',
        'shipping_price', 'discount', 'image_path',
        'description',
    ];

    protected $casts = [
        'extra' => 'array',
    ];

    public function item()
    {
        return $this->morphTo();
    }

}
