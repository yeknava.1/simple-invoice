# Laravel Simple Invoice Package

Laravel Simple Invoice Package.

## Installation

Use the package manager [composer](https://getcomposer.org/) to install simple invoice package.

```bash
composer require yeknava/simple-invoice
```

## Usage
Run this command in your terminal:

```bash
php artisan vendor:publish
```

Add Biller trait to payers models and Add InvoiceOwner trait to payee models if there is any.

```php
<?php

use Yeknava\SimpleInvoice\Biller;

class User extends Model {
    use Biller;
}
```

```php
<?php

use Yeknava\SimpleInvoice\InvoiceOwner;

class OwnerModel extends Model {
    use InvoiceOwner;
}
```

```php
$user = User::find(1);

$user1 = (new UserModel([]));
$user1->save();
$user2 = (new OwnerModel([]));
$user2->save();

$invoice = $user1->newBill('bill title');
$invoice->setOwner($user2);
$invoice->addItem((new InvoiceItem(1000, 'item1'))
    ->setTax(100)
    ->setItem(ItemModel::find(1))
    ->setQuantity(5)
    ->setShippingPrice(100)
    ->setDiscountInPercent(10));
$invoice->addItem((new InvoiceItem(1000, 'item2'))
    ->setTax(100)
    ->setQuantity(5)
    ->setShippingPrice(100)
    ->setDiscountInPercent(10));

$generatedInvoice = $invoice->setExpiredDate(Carbon::now())->generate();
try {
    $generatedInvoice->paid();
} catch (ExpiredInvoiceException $e) {
}

$invoice = $invoice->setExpiredDate(Carbon::now()->addDay())->generate();
$invoice = $invoice->paid();
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License
[MIT](https://choosealicense.com/licenses/mit/)
